<?php
session_start();
$_SESSION['mode'] = 'list';
require 'offlineradio.class.php';
$radio = new OfflineRadio();
$user = $radio->user;
if(!isset($user[$_SERVER['REMOTE_ADDR']])){
echo "ไปเล่นที่อื่นนะจ๊ะ!<br/>";
echo "อยากเล่นก็ไปขอให้ Admin เพิ่ม user ให้สิจ๊ะ<br/>";
exit($_SERVER['REMOTE_ADDR']);
} 
?>
<html>
  <head>
    <title><?= $title ?></title>
    <link rel="stylesheet" href="styles.css">
    <script language="JavaScript" src="jquery-2.1.4.js"></script>
  </head>
  <body>
    <?php if($user[$_SERVER['REMOTE_ADDR']] == "admin"): ?>
    <div id="audio-controls">
        <div id="play-pause">
            <button id="play">PLAY</button>
            <button id="pause">PAUSE</button>
            <button id="next">NEXT</button>
        </div>
        <div id="tracker">
            <div id="progressbar">
                <span id="progress"></span>
            </div>
        </div>
        <div id="duration">0:00</div>
        </div>
    </div>
    <?php endif; ?>
    <div class="clear"></div>
      <div id="place-holder">
          <ul id="playlist"></ul>
      </div>
    <div class="search-file">
        <div class="Library">
	    <iframe height="100%" src="filelist.php"></iframe>
        </div>
    </div>
  </body>
<script language="JavaScript">
    var audio;
    $('#pause').hide();
    function initAudio(element){
        var song = element.data('song');
        console.log(song);
        // Create Audio Object
        audio = new Audio(song);
        
        // if song ended, go to next ==== BROKEN
        $(audio).on('ended', function(){
            control.log("END")
            //$('#next').trigger('click');
        });
    }
    
    // play button
    $('#play').click(function (){
        initAudio($('#playlist li:first-child'));
        audio.play();
        $('#play').hide();
        $('#pause').show();
        $('#duration').fadeIn(400);
        showDuration();
    });
    // pause button
    $('#pause').click(function (){
        audio.pause();
        $('#pause').hide();
        $('#play').show();
        $('#duration').fadeIn(400);
        showDuration();
    });
    // Next button
    $('#next').click(function(){
        audio.pause();
        var next = $('#playlist li:nth-child(2)');
        if(!next){
            return false;
        };
        $('#playlist li:first-child').remove();
        $.ajax({
                url: "nextsong.php",
                data: {mode: "list"},
                type: "post"
        }).done(function (response, textStatus, jqXHR) {
                initAudio(next);
                audio.play();
                showDuration();
                if($('#play').is(':visible')){
                    $('#play').hide();
                    $('#pause').show();
                }
        });
        
    });
    // Time Duration
    var tmpTime = 0;
    function showDuration(){
        $(audio).on('timeupdate', function(){
            var intCurrentTime = parseInt(audio.currentTime);
            var intDuration = parseInt(audio.duration);
            

            if(tmpTime == intCurrentTime){
                return true;
            }

            tmpTime = intCurrentTime;

	    var value = 0;
            if(audio.currentTime > 0){
                value = ((100 / intDuration) * intCurrentTime);
            }
            //get hours $ mins
            var s = parseInt(audio.currentTime % 60);
            var m = parseInt((audio.currentTime)/60) %60;
            
            console.log(parseInt(audio.currentTime), parseInt(audio.duration));
            
            // Add 0 if < 10
            if(s < 10){
                s = '0'+s;
            }
            $('#duration').html(m + ':' + s);
            
	    $("#progress").animate({'width':''+value+'%'}, 1000);
            if(intCurrentTime == intDuration || intCurrentTime == (intDuration - 1)){
                audio.pause();
                setTimeout(function(){ $('#next').click(); }, 1000);
            }
        });
    }


    // click progress bar
    $("#progressbar").mousedown(function(e){
        var leftOffset = e.pageX - $(this).offset().left;
        var songPercents = leftOffset / $('#progressbar').width();
        audio.currentTime = songPercents * audio.duration;
    });
    
    
    jQuery(document).ready(function ($) {
        function setCaption(){
            $.ajax({
                url: "info.php",
                type: "post"
            }).done(function (response, textStatus, jqXHR) {

                if(response!=="null"){

                var data = $.parseJSON(response);
		var strData = "";
		$.each( data, function( key, value ) {
			var style = "color: #000000;";
			if(value.user == "admin") style = "color: #0000FF;"
                        var song_src = "music/"+value.name.replace(/'/g, '%27');
  			strData+="<li data-song='"+song_src+"'><strong style='"+style+"'>"+value.user+"</strong>"+" => "+value.name+"</li>";
//			if($('source[src="'+song_src+'"]').length == 0){
//				
//				$('.my_audio').append("<source id='"+key+"' src='" + song_src + "' type='audio/mpeg'>");
//			}

		});

                $('#playlist').html(strData);

		}
                
                
            })
        }
        setInterval(setCaption,2000);
        
        


//	$('.my_audio').bind('ended', function() { 
//        alert("END!!");
//		$.ajax({
//		        url: "nextsong.php",
//			data: {mode: "list"},
//		        type: "post"
//		}).done(function (response, textStatus, jqXHR) {
//			console.log(response);
//			$('.my_audio').find('source').first().remove();
//			//$("source[id=0]").remove();
//   			$(".my_audio").trigger('load');
//			$(".my_audio").trigger('play');
//   			//play_audio('play');
//            	});
//	});
    });

    function play_audio(task) {
      		if(task == 'play'){
			$.ajax({
				url: "player.php",
				data: {task:'play'},
				type: "post"
			}).done(function (response, textStatus, jqXHR) {
				if(response=="success"){
					$(".my_audio").trigger('play');
				}else{
					alert("ช้าไปนะจ๊ะ!\n"+response);
					return false;
				}
		    	});

			//$(".my_audio").trigger('play');
      		}
      		if(task == 'stop'){
			$.ajax({
				url: "player.php",
				data: {task:'stop'},
				type: "post"
			}).done(function (response, textStatus, jqXHR) {
				if(response=="success"){
					$(".my_audio").trigger('pause');
           				$(".my_audio").prop("currentTime",0);
				}
		    	});
      		}
 	}
</script>
</html>


