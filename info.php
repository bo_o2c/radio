<?php
require 'offlineradio.class.php';
$userip = $_SERVER['REMOTE_ADDR'];
$init = new stdClass();
$init->mp3Folder = dirname(__FILE__).DS.'music';
if(isset($_POST['mode']) && $_POST['mode']=="private"){
    $init->playListName = "playlist/".$userip.'.txt';
}else{
    $init->playListName = 'list.txt';
}
$radio = new OfflineRadio($init);
$output = $radio->getPlaylistItem();
echo json_encode($output);
