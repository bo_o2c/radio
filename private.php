<?php
session_start();
$_SESSION['mode'] = 'private';
require 'offlineradio.class.php';
$radio = new OfflineRadio();
$user = $radio->user;
if(!isset($user[$_SERVER['REMOTE_ADDR']])){
echo "ไปเล่นที่อื่นนะจ๊ะ!<br/>";
echo "อยากเล่นก็ไปขอให้ Admin เพิ่ม user ให้สิจ๊ะ<br/>";

exit($_SERVER['REMOTE_ADDR']);
} 
?>
<html>
  <head>
    <title><?= $title ?></title>
    <link rel="stylesheet" href="styles.css">
    <script language="JavaScript" src="jquery-2.1.4.js"></script>
  </head>
  <body>
    <div class="player"><audio class="my_audio" controls preload="none"></audio></div>
    <div class="control">
        <button onclick="play_audio('play')">PLAY</button>
	<button onclick="play_audio('next')">SKIP</button>
        <button onclick="play_audio('stop')">STOP</button>
    </div>
    <div id="place-holder"></div>
    <div class="search-file">
        <div class="Library">
	    <iframe height="100%" src="filelist.php?mode=private"></iframe>
        </div>
    </div>
  </body>
<script language="JavaScript">
    jQuery(document).ready(function ($) {
	
        function setCaption(){
            $.ajax({
                url: "info.php",
		data: {mode: "private"},
                type: "post"
            }).done(function (response, textStatus, jqXHR) {

                if(response!=="null"){

                var data = $.parseJSON(response);
		var strData = "";
		$.each( data, function( key, value ) {
			var style = "color: #000000;";
			if(value.user == "admin") style = "color: #0000FF;"
  			strData+="<p><strong style='"+style+"'>"+value.user+"</strong>"+" => "+value.name+"<br /></p>";
			var song_src = "music/"+value.name.replace(/'/g, '%27');
			if($('source[src="'+song_src+'"]').length == 0){
				
				$('.my_audio').append("<source id='"+key+"' src='" + song_src + "' type='audio/mpeg'>");
			}

		});

                $('#place-holder').html(strData);

		}
            })
        }
        setInterval(setCaption,2000);

	$('.my_audio').on('ended', function() { 

		$.ajax({
		        url: "nextsong.php",
			data: {mode: "private"},
		        type: "post",
			async: false
		}).done(function (response, textStatus, jqXHR) {
			$('.my_audio').find('source').first().remove();
   			$(".my_audio").trigger('load');
			$(".my_audio").trigger('play');
   			//play_audio('play');
            	});
	});
    });

    function play_audio(task) {
      		if(task == 'play'){
			$(".my_audio").trigger('play');
      		}
      		if(task == 'stop'){
			$(".my_audio").trigger('pause');
           		$(".my_audio").prop("currentTime",0);
      		}
		if(task == 'next'){
			$(".my_audio").trigger('pause');
           		$(".my_audio").prop("currentTime",0);
			
			$.ajax({
				url: "nextsong.php",
				data: {mode: "private"},
				type: "post",
				async: false
			}).done(function (response, textStatus, jqXHR) {
				$('.my_audio').find('source').first().remove();
	   			$(".my_audio").trigger('load');
				$(".my_audio").trigger('play');
		    	});
      		}
 	}
</script>
</html>


